import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Button;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeliveryPersonMenu {

	private JFrame frame;
	private DatabaseConnector dbc;
	private static String nm;

	/**
	 * Launch the application.
	 */
	public static void DeliveryPersonMenuScreen(DatabaseConnector dbobj, String str) {
		nm = str;
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryPersonMenu window = new DeliveryPersonMenu(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public DeliveryPersonMenu(DatabaseConnector dbc) throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		dbc = new DatabaseConnector();
		frame = new JFrame("Delivery Person Menu");
		frame.getContentPane().setFont(new Font("Tahoma", Font.BOLD, 13));
		frame.setBounds(100, 100, 471, 279);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		Button button = new Button("View My Account");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ViewDeliveryDetails nw=new ViewDeliveryDetails(dbc);
					nw.DeliveryDetailsScreen(dbc, nm);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button.setFont(new Font("Dialog", Font.BOLD, 18));
		button.setBounds(129, 89, 180, 52);
		frame.getContentPane().add(button);
		
		JLabel lblDeliveryPersonMenu = new JLabel("Delivery Person Menu");
		lblDeliveryPersonMenu.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDeliveryPersonMenu.setBounds(117, 11, 220, 42);
		frame.getContentPane().add(lblDeliveryPersonMenu);
		
		Button button_1 = new Button("<< Return ");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					StartPage nw=new StartPage(dbc);
					nw.StartScreen(dbc);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_1.setBounds(345, 198, 79, 33);
		frame.getContentPane().add(button_1);
	}
}
