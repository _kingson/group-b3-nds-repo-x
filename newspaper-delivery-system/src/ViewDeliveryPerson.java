import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewDeliveryPerson {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void ViewDeliveryPersonScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewDeliveryPerson window = new ViewDeliveryPerson(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @param dbobj 
	 */
	public ViewDeliveryPerson(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("View Delivery Person");
		frame.setBounds(100, 100, 508, 589);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblViewCustomers = new JLabel("View Delivery Person");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(140, 11, 224, 31);
		frame.getContentPane().add(lblViewCustomers);
		
		textField = new JTextField();
		textField.setBounds(140, 68, 189, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblName = new JLabel("Delivery Person:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 118, 17);
		frame.getContentPane().add(lblName);
		
		Button button = new Button("Search");
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);
		
		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 172, 78, 17);
		frame.getContentPane().add(lblName_1);
		
		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 111, 78, 31);
		frame.getContentPane().add(lblDetails);
		
		JLabel lblNewLabel = new JLabel("Seach for delivery person...");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(140, 175, 224, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setBounds(10, 270, 78, 17);
		frame.getContentPane().add(lblAddress);
		
		JTextArea txtrSearchForCustomer = new JTextArea();
		txtrSearchForCustomer.setEnabled(false);
		txtrSearchForCustomer.setLineWrap(true);
		txtrSearchForCustomer.setText("Search for delivery person...");
		txtrSearchForCustomer.setBounds(140, 268, 189, 60);
		frame.getContentPane().add(txtrSearchForCustomer);
		
		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 349, 78, 17);
		frame.getContentPane().add(lblPhone);
		
		JLabel lblSeachForDelivery = new JLabel("Seach for delivery person...");
		lblSeachForDelivery.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSeachForDelivery.setBounds(140, 351, 224, 14);
		frame.getContentPane().add(lblSeachForDelivery);
		
		JLabel lblPublications = new JLabel("Username:");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 218, 96, 17);
		frame.getContentPane().add(lblPublications);
		
		Button button_1 = new Button("<< Previous");
		button_1.setBounds(243, 461, 86, 22);
		frame.getContentPane().add(button_1);
		
		Button button_2 = new Button("Next >>");
		button_2.setBounds(335, 461, 86, 22);
		frame.getContentPane().add(button_2);
		
		JLabel label = new JLabel("Seach for delivery person...");
		label.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label.setBounds(140, 221, 224, 14);
		frame.getContentPane().add(label);
		
		Button button_3 = new Button("<< Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					DeliveryChoose nw=new DeliveryChoose(dbobj);
					nw.NewScreen5(dbobj);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
					try {
						frame.setVisible(false);
					} catch (Exception ex1) {
						ex1.printStackTrace();
					}
				
			}
		});
		button_3.setBounds(34, 461, 86, 22);
		frame.getContentPane().add(button_3);
		
		JLabel lblAssignedRegion = new JLabel("Assigned Region:");
		lblAssignedRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAssignedRegion.setBounds(10, 402, 118, 17);
		frame.getContentPane().add(lblAssignedRegion);
		
		JLabel label_1 = new JLabel("Seach for delivery person...");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(140, 405, 224, 14);
		frame.getContentPane().add(label_1);
	}
}
